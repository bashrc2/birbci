APP=birbci
PREFIX?=/usr/local

all:

debug:

install:
	cp ${APP} ${DESTDIR}${PREFIX}/bin
	mkdir -p ${DESTDIR}/usr/share/${APP}/images
	cp -r images/* ${DESTDIR}/usr/share/${APP}/images
	mkdir -m 755 -p ${DESTDIR}${PREFIX}/share/man/man1
	cp man/${APP}.1.gz ${DESTDIR}${PREFIX}/share/man/man1

uninstall:
	rm ${PREFIX}/bin/${APP}
	rm -rf /usr/share/${APP}
	rm ${PREFIX}/share/man/man1/${APP}.1.gz
