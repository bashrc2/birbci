#!/bin/bash
#
# Birb CI - The minimal Continuous Integration system
#
# License
# =======
#
# Copyright (C) 2018 Bob Mottram <bob@freedombone.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

APP_NAME=birbci

export TEXTDOMAIN=${APP_NAME}
export TEXTDOMAINDIR="/usr/share/locale"

BUILD_DIR=
REPO=
BRANCH=
BIRB_FILE=.birb
BUILD_SCRIPT=
TIMEOUT_MINS=60
DAEMON=
WEB_FILE=
HEADING=$'Birb Continuous Integration Build Results'
EMAIL_ADDRESS=
XMPP_ADDRESS=
COPY_IMAGES=
SYNCHRONOUS=
USE_BUILD_LOG=no
UPDATE_SEC=300
REBOOT_ON_BUILD_FAIL=

function create_daemon {
    daemon="$1"

    # Create a systemd daemon which will run builds for this app
    if [ ! -d /etc/${APP_NAME} ]; then
        mkdir /etc/${APP_NAME}
    fi

    # Create an unprivileged user to run the daemon as
    if [ ! "$(id -u ${APP_NAME})" ]; then
        useradd -d /etc/${APP_NAME}/ -s /bin/false ${APP_NAME}
    fi

    BUILD_DIR="/etc/${APP_NAME}/$daemon"

    # Set a default output html file to show build status
    if [ ! "$WEB_FILE" ]; then
        WEB_FILE=/var/www/html/index.html
    fi

    # Copy images and icons to the html file location
    if [ -d /usr/share/${APP_NAME}/images ]; then
        IMAGES_DIR=$(dirname "${WEB_FILE}")/${APP_NAME}_img
        if [ ! -d "$IMAGES_DIR" ]; then
            mkdir "$IMAGES_DIR"
            cp /usr/share/${APP_NAME}/images/* "$IMAGES_DIR"
        else
            # force the update of images
            if [[ "$COPY_IMAGES" == "y"* || "$COPY_IMAGES" == "Y"* ]]; then
                cp /usr/share/${APP_NAME}/images/* "$IMAGES_DIR"
            fi
        fi
    fi

    # add daemon to list
    if [ -f "/etc/${APP_NAME}/builds.txt" ]; then
        if ! grep -q "$daemon," "/etc/${APP_NAME}/builds.txt"; then
            echo "$daemon,$BUILD_DIR,$REPO,$BRANCH,$BUILD_SCRIPT,$WEB_FILE,$XMPP_ADDRESS,$USE_BUILD_LOG" >> "/etc/${APP_NAME}/builds.txt"
        else
            sed -i "s|$daemon,.*|$daemon,$BUILD_DIR,$REPO,$BRANCH,$BUILD_SCRIPT,$WEB_FILE,$XMPP_ADDRESS,$USE_BUILD_LOG|g" "/etc/${APP_NAME}/builds.txt"
        fi
    else
        echo "$daemon,$BUILD_DIR,$REPO,$BRANCH,$BUILD_SCRIPT,$WEB_FILE,$XMPP_ADDRESS,$USE_BUILD_LOG" > "/etc/${APP_NAME}/builds.txt"
    fi

    # create script to be run via cron
    { echo '#!/bin/bash';
      echo '';
      echo 'while true';
      echo 'do';
      echo "    sleep $UPDATE_SEC";
      echo '';
      echo "    if [ ! -f /etc/${APP_NAME}/builds.txt ]; then";
      echo '        continue';
      echo '    fi';
      echo '';
      echo "    existing_builds=\$(ps aux | grep \"${APP_NAME} -d\" | grep -vc \"grep\")";
      echo "    if [ \$existing_builds -eq 0 ]; then";
      echo "        no_of_builds=\$(cat /etc/${APP_NAME}/builds.txt | wc -l)";
      echo "        line_number=\$((RANDOM % \$no_of_builds))";
      echo "        build_name=\$(sed \"\${line_number}q;d\" /etc/${APP_NAME}/builds.txt | awk -F ',' '{print \$1}')";
      echo "        BUILD_DIR=\$(sed \"\${line_number}q;d\" /etc/${APP_NAME}/builds.txt | awk -F ',' '{print \$2}')";
      echo "        REPO=\$(sed \"\${line_number}q;d\" /etc/${APP_NAME}/builds.txt | awk -F ',' '{print \$3}')";
      echo "        BRANCH=\$(sed \"\${line_number}q;d\" /etc/${APP_NAME}/builds.txt | awk -F ',' '{print \$4}')";
      echo "        BUILD_SCRIPT=\$(sed \"\${line_number}q;d\" /etc/${APP_NAME}/builds.txt | awk -F ',' '{print \$5}')";
      echo "        WEB_FILE=\$(sed \"\${line_number}q;d\" /etc/${APP_NAME}/builds.txt | awk -F ',' '{print \$6}')";
      echo "        XMPP_ADDRESS=\$(sed \"\${line_number}q;d\" /etc/${APP_NAME}/builds.txt | awk -F ',' '{print \$7}')";
      echo "        USE_BUILD_LOG=\$(sed \"\${line_number}q;d\" /etc/${APP_NAME}/builds.txt | awk -F ',' '{print \$8}')";
      echo "        /usr/local/bin/${APP_NAME} -d \"\$BUILD_DIR\" -r \"\$REPO\" -b \"\$BRANCH\" -s \"\$BUILD_SCRIPT\" -w \"\$WEB_FILE\" -x \"\$XMPP_ADDRESS\" --build-log \$USE_BUILD_LOG";
      echo '    fi';
      echo 'done'; } > /usr/bin/birbci-daemon
    chmod +x /usr/bin/birbci-daemon

    { echo '[Unit]';
      echo 'Description=BirbCI: the minimal CI system';
      echo 'After=getty@tty6.service';
      echo '';
      echo '[Service]';
      echo 'User=root';
      echo 'ExecStart=/usr/bin/birbci-daemon';
      echo "ExecReload=/bin/kill \$MAINPID";
      echo 'StandardInput=tty';
      echo 'TTYPath=/dev/tty6';
      echo 'TTYReset=yes';
      echo 'TTYVHangup=yes';
      echo 'KillMode=process';
      echo 'Restart=always';
      echo '';
      echo '[Install]';
      echo 'WantedBy=default.target'; } > /etc/systemd/system/birbci.service
    systemctl enable birbci.service
    systemctl daemon-reload
    systemctl restart birbci.service

    # Set permissions to the unprivileged user
    chown -R ${APP_NAME}: /etc/${APP_NAME}
}

function show_help {
    echo ''
    echo $"${APP_NAME} -d [directory] --repo [git repo url] --branch [branch] --script [build script] --install [daemon name] -w [html file] --remove [daemon name] --header [text] -e [email address] --images [yes|no] --xmpp [adminJID] --synchronous [yes|no] --build-log [yes|no] --rebootonfail [yes|no]"
    echo ''
    exit 0
}

function xmpp_send {
    XMPP_ADDRESS="$1"
    MESSAGE="$2"

    if [[ "$XMPP_ADDRESS" != *'@'* ]]; then
        return
    fi

    if [ -d /etc/prosody ]; then
        notification_user_password=$(openssl rand -base64 32 | tr -dc A-Za-z0-9 | head -c 30 ; echo -n '')
        if prosodyctl register "notification" "$HOSTNAME" "$notification_user_password"; then
            { echo '#!/usr/bin/python2';
              echo 'import sys,os,xmpp,time';
              echo 'tojid = sys.argv[1]';
              echo 'data = sys.stdin.readlines()';
              echo "msg = ' '.join(data)";
              echo "username = 'notification@$HOSTNAME'";
              echo "password = '$notification_user_password'";
              echo 'jid=xmpp.protocol.JID(username)';
              echo 'cl=xmpp.Client(jid.getDomain(),debug=[])';
              echo 'con=cl.connect()';
              echo 'if not con:';
              echo '    sys.exit()';
              echo 'auth=cl.auth(jid.getNode(),password,resource=jid.getResource())';
              echo 'if not auth:';
              echo '    sys.exit()';
              echo 'id=cl.send(xmpp.protocol.Message(tojid, msg))';
              echo 'time.sleep(1)';
              echo 'cl.disconnect()'; } > /tmp/xsend
            chmod +x /tmp/xsend

            echo "$MESSAGE" | python2 /tmp/xsend "$XMPP_ADDRESS"
            systemctl restart prosody
            rm /tmp/xsend
        fi
        prosodyctl deluser "notification@$HOSTNAME"
    fi
}

function create_web_page {
    filename="$1"

    if [ ! "$filename" ]; then
        return
    fi

    # get the directory part of the filename
    web_dir=$(dirname "${filename}")

    template_filename="${web_dir}/birbci_template.html"

    # Start of the html file
    if [ -f "$template_filename" ]; then
        # if a template exists
        cp "$template_filename" "$filename"
        sed -i '/</table>/d' "$filename"
        sed -i '/</body>/d' "$filename"
        sed -i '/</html>/d' "$filename"
    else
        { echo '<html>';
          echo "<title>$APP_NAME build status</title>";
          echo "<body background=\"${APP_NAME}_img/background.jpg\">";
          echo '<meta http-equiv="refresh" content="60">';
          echo '<center>';
          echo "<h1>$HEADING</h1>";
          echo '</center>';
          echo '<hr>';
          echo '<table style="width:100%" border="0">'; } > "$filename"
    fi

    # for each app in the CI system
    cd /etc/${APP_NAME} || return
    for d in */ ; do

        # If a results (birb) file exists
        results="${d}${BIRB_FILE}"

        if [ -f "$results" ]; then

            # Get some info from the results file
            status=$(grep "status:" "$results" | awk -F ':' '{print $2}')
            end_date=$(grep "end:" "$results" | sed 's|end:||g')
            # shellcheck disable=SC2001
            test_dir=$(echo "$d" | sed 's|/||g')
            CURR_COMMIT=$(grep "head:" "$results" | awk -F ':' '{print $2}' | tr -d '[:space:]')

            # get the colour corresponding to the build status
            text_color='green'
            last_good_commit_str=''
            if [[ "$status" == 'building' ]]; then
                text_color='gray'
            fi
            if [[ "$status" == 'fail'* ]]; then
                text_color='red'
                last_good_commit=$(grep "commit:" "$results" | awk -F ':' '{print $2}' | tr -d '[:space:]')
                if [ "$last_good_commit" ]; then
                    if [[ "$last_good_commit" != "$CURR_COMMIT" ]]; then
                        last_good_commit_str="<br><font color=\"green\">$last_good_commit</font>"
                    else
                        text_color='green'
                    fi
                fi
            fi

            if [[ "$USE_BUILD_LOG" != 'y'* ]]; then
                # The default build log
                build_log_filename="build_${test_dir}.txt"
            else
                # build log generated by another system
                build_log_filename="buildlog_${test_dir}.txt"
            fi
            if [ ! -f "${web_dir}/${build_log_filename}" ]; then
                echo $'There are no build results yet' > "${web_dir}/${build_log_filename}"
            fi

            # Add an entry to the html table
            { echo "<tr>";
              echo "  <th>";
              echo "    <a href=\"./downloads\"><img src=\"${APP_NAME}_img/${status}.png\" width=\"64\" alt=\"${status}\"/></a>";
              echo "  </th>";
              echo "  <th>";
              echo "    <a href=\"${build_log_filename}\">$test_dir</a>";
              echo "  </th>";
              echo "  <th>";
              echo "    <font color=\"${text_color}\">$CURR_COMMIT</font>${last_good_commit_str}";
              echo "  </th>";
              echo "  <th>";
              echo "    <font color=\"$text_color\">${end_date}</font>";
              echo "  </th>";
              echo "</tr>"; } >> "$filename"
        fi
    done

    # End of the html file
    { echo '</table>';
      echo '</body>';
      echo '</html>'; } >> "$filename"

    chown -R www-data:www-data "$web_dir"
}

function update {
    if [[ "$SYNCHRONOUS" == 'y'* || "$SYNCHRONOUS" == '1' || "$SYNCHRONOUS" == 'on' ]]; then
        # shellcheck disable=SC2009
        existing_builds=$(ps aux | grep "birbci -d" | grep -vc "grep")
        # shellcheck disable=SC2086
        if [ $existing_builds -gt 1 ]; then
            return
        fi
    fi

    # If the repo isn't cloned then do that now
    if [ ! -d "$BUILD_DIR" ]; then
        if ! git clone "$REPO" "$BUILD_DIR"; then
            echo $"Failed to clone repo $REPO"
            if [ -d "$BUILD_DIR" ]; then
                rm -rf "$BUILD_DIR"
            fi
            exit 57835683
        fi
    fi

    # Update the repo to the latest commit
    cd "$BUILD_DIR" || exit 727284
    if [ ! -d .git ]; then
        echo $'No git repo found'
        exit 6728542
    fi
    git stash
    git checkout "$BRANCH"
    git pull

    # Get the current commit for the head of the repo
    CURR_COMMIT=$(git log -n 1 | grep commit | awk -F ' ' '{print $2}' | tr -d '[:space:]')

    # No build is currently in progress
    PREV_COMMIT=

    # If previous results exist
    if [ -f $BIRB_FILE ]; then

        # Get the last commit for which a build was attempted
        LAST_COMMIT_TRIED=$(grep 'last:' $BIRB_FILE | awk -F ':' '{print $2}' | tr -d '[:space:]')

        # If this is the same as the current head then do nothing
        if [[ "$LAST_COMMIT_TRIED" == "$CURR_COMMIT" ]]; then
            # nothing more to try
            return
        fi

        # Get the currently in progress build commit
        PREV_COMMIT=$(grep 'commit:' $BIRB_FILE | awk -F ':' '{print $2}' | tr -d '[:space:]')
    fi

    # If the currently in progress build is the head then do nothing
    if [[ "$PREV_COMMIT" == "$CURR_COMMIT" ]]; then
        return
    fi

    if [ -f $BIRB_FILE ]; then

        # If this app is still building
        if grep -q "status:building" $BIRB_FILE; then

            # How long has it been building for?
            PREV_BUILD_START=$(grep 'start:' $BIRB_FILE | awk -F ':' '{print $2}')
            CURR_TIME=$(date +%s)
            DIFF_MINS=$(echo $((CURR_TIME-PREV_BUILD_START)) | awk '{print int($1/60)}')

            # If the build was only started recently then do nothing
            if [ "$DIFF_MINS" -lt $TIMEOUT_MINS ]; then
                return
            fi
        fi

        # set the starting time and status for the build
        sed -i "s|head:.*|head:${CURR_COMMIT}|g" $BIRB_FILE
        sed -i "s|start:.*|start:$(date +%s)|g" $BIRB_FILE
        sed -i "s|status:.*|status:building|g" $BIRB_FILE
    else
        # create a new results file for this app
        { echo "commit:";
          echo "head:${CURR_COMMIT}";
          echo "start:$(date +%s)";
          echo "end:";
          echo "last:";
          echo "status:building"; } > $BIRB_FILE
    fi

    # Create the web page before the build starts, so that we know
    # what's going on
    create_web_page "$WEB_FILE"
    cd "$BUILD_DIR" || exit 567353

    if [[ "$BUILD_SCRIPT" != 'make'* ]]; then
        # If it's not just a make command then set the build script as executable
        chmod +x $BUILD_SCRIPT
    fi

    # Now run the build script for the app
    build_success=
    if $BUILD_SCRIPT; then
        # If the build succeeds
        sed -i "s|status:.*|status:success|g" $BIRB_FILE
        sed -i "s|commit:.*|commit:${CURR_COMMIT}|g" $BIRB_FILE
        build_success=1
    else
        # If the build fails
        sed -i "s|status:.*|status:failed|g" $BIRB_FILE
    fi

    # Whether or not the build succeeds set the last commit tried and end time
    sed -i "s|last:.*|last:${CURR_COMMIT}|g" $BIRB_FILE
    sed -i "s|end:.*|end:$(date '+%Y-%m-%d %H:%M:%S')|g" $BIRB_FILE

    if [[ "$BUILD_DIR" == "/etc/${APP_NAME}/"* ]]; then

        # Save the systemd journal so that any build failures can be seen
        # via the web page
        DAEMON=$(basename "$BUILD_DIR")
        journalctl -u "$DAEMON" > "/etc/${APP_NAME}/${DAEMON}/build.txt"

        if [ ! $build_success ]; then

            # If the build failed then optionally send an email
            if [ "$EMAIL_ADDRESS" ]; then
                mail -s "[Birb CI] ${DAEMON} build failure" "$EMAIL_ADDRESS" < "/etc/${APP_NAME}/${DAEMON}/build.txt"
            fi

            if [ "$XMPP_ADDRESS" ]; then
                msgstr=$(echo -e "[Birb CI] ${DAEMON} build failure\\n\\n")
                msgstr=$msgstr$(cat "/etc/${APP_NAME}/${DAEMON}/build.txt")
                xmpp_send "$XMPP_ADDRESS" "$msgstr"
            fi
        fi
    fi

    # Create the web page after the build result is known
    create_web_page "$WEB_FILE"

    if [ ! $build_success ]; then
        if [[ "$REBOOT_ON_BUILD_FAIL" == 'y'* || "$REBOOT_ON_BUILD_FAIL" == '1' ]]; then
            systemctl reboot -i
        fi
    fi
}

while [ $# -gt 1 ]
do
key="$1"

case $key in
    -h|--help)
    show_help
    ;;
    -d|--dir|--directory)
    shift
    BUILD_DIR="$1"
    ;;
    -r|--repo|--repository)
    shift
    REPO="$1"
    ;;
    --build-log)
    shift
    USE_BUILD_LOG="$1"
    ;;
    -b|--branch)
    shift
    BRANCH="$1"
    ;;
    -s|--build|--script)
    shift
    BUILD_SCRIPT="$1"
    ;;
    -i|--install|--daemon)
    shift
    DAEMON="$1"
    ;;
    -w|--web)
    shift
    WEB_FILE="$1"
    ;;
    --remove)
    shift
    REMOVE_DAEMON="$1"
    ;;
    --header|--heading)
    shift
    HEADING="$1"
    ;;
    -e|--email|--mail)
    shift
    EMAIL_ADDRESS="$1"
    ;;
    -x|--jid|--xmpp)
    shift
    XMPP_ADDRESS="$1"
    ;;
    --images|--copyimages)
    shift
    COPY_IMAGES="$1"
    ;;
    --synchronous|--noparallel)
    shift
    SYNCHRONOUS="$1"
    ;;
    --rebootonfail)
    shift
    REBOOT_ON_BUILD_FAIL="$1"
    ;;
    *)
    # unknown option
    ;;
esac
shift
done

if [ "$REMOVE_DAEMON" ]; then
    if [ -f /etc/systemd/system/birbci.service ]; then
        systemctl stop birbci.service
        systemctl disable birbci.service
        rm /etc/systemd/system/birbci.service
    fi

    if [ -f /etc/${APP_NAME}/builds.txt ]; then
        sed -i "/${REMOVE_DAEMON},/d" /etc/${APP_NAME}/builds.txt
    fi

    if [ -d "/etc/$APP_NAME/$REMOVE_DAEMON" ]; then
        rm -rf "/etc/${APP_NAME:?}/$REMOVE_DAEMON"
    fi
    if [ -d "/etc/${APP_NAME}/${REMOVE_DAEMON}-build" ]; then
        rm -rf "/etc/${APP_NAME}/${REMOVE_DAEMON}-build"
    fi
    if [ -f "/etc/${APP_NAME}/${REMOVE_DAEMON}_build.log" ]; then
        rm "/etc/${APP_NAME}/${REMOVE_DAEMON}_build.log"
    fi
    exit 0
fi

if [ ! "$REPO" ]; then
    echo $'No git repo specified'
    exit 7892462
fi

if [ ! "$DAEMON" ]; then
    if [ ! "$BUILD_DIR" ]; then
        echo $'No build directory specified'
        exit 3798684
    fi
fi

if [ ! "$BUILD_SCRIPT" ]; then
    echo $"No build script was specified"
    exit 8346345
fi

if [ ! -f "$BUILD_SCRIPT" ]; then
    echo $"No build script $BUILD_SCRIPT"
    exit 3798684
fi

if [ ! "$BRANCH" ]; then
    echo $'No branch specified. Default to master.'
    BRANCH=master
fi

if [ "$DAEMON" ]; then
    create_daemon "$DAEMON"
else
    # Run the build
    update
fi

exit 0
