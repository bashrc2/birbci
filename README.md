<img src="https://code.freedombone.net/bashrc/birbci/raw/master/images/logo.png?raw=true" width=256/>


# Birb CI: The minimal Continuous Integration system

This is a minimalistic continuous integration system which will periodically try to build a given set of target projects and report the status.

## Installation

Install some dependencies. On a Debian based system:

``` bash
sudo apt-get install git build-essential nginx python-xmpp
```

Or an an Arch/Parabola based system:

``` bash
sudo pacman -S git nginx python2
git clone https://github.com/normanr/xmpppy
cd xmpppy
sudo python2 setup.py install
cd ..
```

Nginx is optional. You could use a different web server, or just view the html file created to display build statuses directly within a browser.

Then install birbci from the repo:

``` bash
git clone https://code.freedombone.net/bashrc/birbci
cd birbci
sudo make install
```

## Adding Builds

Create a script to build the application which you want to test, such as:

``` bash
#!/bin/bash
set -e
set -x
make
```

Rather than just knowing whether the target project compiles you might also want to run any unit tests within the build script and have it return a non-zero value if any tests fail.

So that it's accessible to the build system you may want to save this script into a directory called */etc/birbci*

Install a target to be built with:

``` bash
sudo birbci --repo [your git repo url] --branch master --script /etc/birbci/build.sh -w /var/www/html/index.html --install [build name]
```

The build name could be similar to the name of the system being tested, but not identical so as to avoid potential clashes. The *-w* option defines a html file which will contain the results.

The above install could be performed multiple times for different test repos with different build scripts.

Optionally you can also use the *--email* option to specify an email address which will be notified if a build failure occurs. Whether you use this depends upon if you have an email server locally installed.

## XMPP notifications

If you wish to receive build failure notifications via XMPP then the --xmpp option can be used. For this to work you'll need to have prosody installed and configured.

``` bash
sudo birbci --repo [your git repo url] --branch master --script /etc/birbci/build.sh -w /var/www/html/index.html --install [build name] --xmpp user@xmppdomain
```
## Removing Builds

If you later want to remove a repo from the CI system:

``` bash
sudo birbci --remove [build name]
```

## Viewing the Current Build Status

To view the results navigate to or open the html file. The result should look something like this, with the colours of the icons indicating success (green), failure (red) or building (gray).

## Customizing the Web Page

You can also customize the web page if you want it to look different or have your own branding. Copy the **birbci_template.html** file into your **/var/www/html** directory and customize it as needed. Don't add anything below the table, but anything above can be changed.

<img src="https://code.freedombone.net/bashrc/birbci/raw/master/images/birbciresults.png?raw=true" width=800/>

Selecting the build name will show the build log, so that you can view any compile errors or unit test failures.
